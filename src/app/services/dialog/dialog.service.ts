import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ModalComponent } from 'src/app/directives/modal/modal.component';
import { messages } from '../../configs/messages';
@Injectable({
  providedIn: 'root'
})
export class DialogService {
  private profileName = new Subject<any>();
  pname = this.profileName.asObservable();
  dialogRef: any;
  public static loggedInUser = '';
  modalOptions: any = {
    default: { width: '350px', maxHeight: '450px' },
    confirm: { width: '450px', maxHeight: '250px' }
  };

  constructor(private dialog: MatDialog, public router: Router) { }

  // close dialog
  closeDialog() {
    this.dialogRef.close();
  }
  //canDeactivate confirm message
  canDeactivateConfirm(): Observable<boolean> | boolean | Promise<boolean> {
    return new Promise((resolve, reject) => {
      const dialogRef = this.dialog.open(ModalComponent, {
        width: '450px',
        disableClose: true,
        data: {
          dialogTitle: messages.confirmHeading,
          message: messages.deactivateConfirmMsg,
          buttons: [
            {
              label: 'Stay',
              handler: () => dialogRef.close('Stay')
            },
            {
              label: 'Leave',
              active: true,
              handler: () => {
                dialogRef.close('Leave');
              }

            }
          ]
        }
      });

      dialogRef.afterClosed().subscribe(closeType => {
        if (closeType && closeType === 'Stay') { // Stay click
          resolve(false);
        } else { // Ok leave
          resolve(true);
        }
      }, () => {
        reject(false);
      });

    });

  }
  // Open confirm dialog
  openConfirmDialog(options) {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '350px',
      data: {
        ...options,
        buttons: [
          {
            label: 'No',
            handler: () => dialogRef.close('No')
          },
          {
            label: 'Yes',
            active: true,
            handler: () => {
              dialogRef.close('Yes');
            }
          }
        ]
      }
    });
    return dialogRef;
  }
  getProfile(name: string) {
    this.profileName.next(name);

  }
}
