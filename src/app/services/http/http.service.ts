import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient: HttpClient) { }

  get(url) {
    return this.httpClient.get(url);
  }
  post(url, data) {
    return this.httpClient.post(url, data);
  }
  put(url, data) {
    console.log("Put:" + url);
    return this.httpClient.put(url, data);
  }
  delete(url) {
    return this.httpClient.delete(url);
  }

}
