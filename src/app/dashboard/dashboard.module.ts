import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../material/material.module';
import { HomeComponent } from '../dashboard/home/home.component';
import { AboutComponent } from '../dashboard/about/about.component';
import { ContactComponent } from '../dashboard/contact/contact.component';
import { UserslistComponent } from '../dashboard/userslist/userslist.component';
import { UserDetailsComponent } from '../dashboard/userslist/user-details/user-details.component';
import { CanDeActivateGuard } from '../guards/can-deactivate.guard';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { ProfileComponent } from '../dashboard/profile/profile.component';
export const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'about',
        loadChildren: () => import('./about/about.module').then(m => m.AboutModule),
      },
      {
        path: 'contact',
        loadChildren: () => import('./contact/contact.module').then(m => m.ContactModule),
      },
      {
        path: 'users_table',
        loadChildren: () => import('./userslist/userslist.module').then(m => m.UserslistModule),

      },
      { path: 'users_table/:userid', component: UserDetailsComponent },
      { path: 'profile', component: ProfileComponent, canDeactivate: [CanDeActivateGuard] }

    ]

  }

];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule
  ],
  declarations: [DashboardComponent, HomeComponent],
  providers: [CanDeActivateGuard],
})
export class DashboardModule { }

