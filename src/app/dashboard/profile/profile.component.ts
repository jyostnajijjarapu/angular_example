import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { CanDeActivateGuard } from 'src/app/guards/can-deactivate.guard';
import { DashboardComponent } from '../dashboard.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, CanDeActivateGuard {
  userProfile: FormGroup;
  profile: string;
  userName: string;
  profileMsg: string;
  constructor(private router: Router, private dialogService: DialogService) {

    this.userProfile = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      mobileNo: new FormControl('', [Validators.required])
    });
  }
  ngOnInit() {
  }
  profileSubmit() {

    if (this.userProfile.valid) {
      DialogService.loggedInUser = this.userProfile.controls.firstName.value;
      this.profileMsg = DialogService.loggedInUser;
      this.dialogService.getProfile(this.profileMsg);

      console.log(this.profileMsg);
      console.log(DialogService.loggedInUser);


    }
  }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.userProfile.dirty && !this.userProfile.valid) {
      return this.dialogService.canDeactivateConfirm();
    }
    else {
      return true;
    }

  }

}