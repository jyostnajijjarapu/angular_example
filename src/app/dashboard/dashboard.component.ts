import { Component, OnInit, Input, Injectable, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { messages } from 'src/app/configs/messages';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { ProfileComponent } from 'src/app/dashboard/profile/profile.component';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = 'matapp';
  userName: any;
  profile: any;
  pName: string;

  constructor(public dialog: MatDialog, public dialogService: DialogService, private router: Router) {
    this.userName = DialogService.loggedInUser;
    this.profile = this.userName.charAt(0).toUpperCase();

  }

  logoutHandler() {
    const options = {
      dialogTitle: messages.logoutHeadingMsg,
      message: messages.logoutMsg
    }
    const dialogRef = this.dialogService.openConfirmDialog(options);
    dialogRef.afterClosed().subscribe(closeType => {
      if (closeType && closeType === 'Yes') { // Cancel click
        this.loggedout();
      }
    });
  }

  loggedout() {
    this.router.navigate(['']);
  }

  ngOnInit(): void {
    this.dialogService.pname.subscribe(name => {
      this.userName = name;
      this.profile = this.userName.charAt(0).toUpperCase();
    })
  }
  // ngAfterViewInit() {
  //   this.profileMsg = this.profilename.profileMsg;
  //   this.userName = this.profileMsg;
  //   // this.userName = DialogService.loggedInUser;
  //   this.profile = this.userName.charAt(0).toUpperCase();
  // }
  // // refreshProfileName(event: any) {
  //   console.log("refreshProfileName");
  //   this.userName = DialogService.loggedInUser;
  //   this.profile = this.userName.charAt(0).toUpperCase();
  // }



}
