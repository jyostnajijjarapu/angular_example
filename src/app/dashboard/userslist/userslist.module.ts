import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserslistComponent } from './userslist.component';
const routes: Routes = [
    {
        path: '',
        component: UserslistComponent
    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserslistModule { }