import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { UserComponent } from './user/user.component';
import { HttpService } from 'src/app/services/http/http.service';
import { messages } from 'src/app/configs/messages';
import { DialogService } from 'src/app/services/dialog/dialog.service';
import { LoggerService } from 'src/app/services/logger/logger.service';
@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.css']
})
export class UserslistComponent implements OnInit {

  serviceUrl = 'users';
  tableColumns: string[] = ['id', 'created_at', 'firstName', 'lastName', 'email', 'hobbies', 'address.city', 'address.state', 'address.pincode', 'action'];
  isLoading = false;
  userList;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public httpClient: HttpClient,
    public dialog: MatDialog,
    public http: HttpService,
    public dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this.refreshUserList();
  }

  refreshUserList() {

    this.isLoading = true;
    LoggerService.print("In UserTable refreshUserList Calling http get:");
    this.http.get('users').subscribe(
      result => {
        this.userList = result;
        for (let user of this.userList) {
          user.created_at = moment.unix(user.created_at).format("YYYY-MM-DD");
        }
        this.isLoading = false;
        this.userList = new MatTableDataSource(this.userList);
        this.userList.paginator = this.paginator;

      },
      error => {
        this.isLoading = false;
        const options = {
          dialogTitle: messages.errorHeading,
          message: messages.serverErrorMsg,
        }
        const dialogRef = this.dialogService.openConfirmDialog(options);
        dialogRef.afterClosed().subscribe(closeType => {
          if (closeType && closeType === 'Yes') {
            this.dialogService.closeDialog();
          }
        });
      }
    );
  }
  onCreate() {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.data = "createUser";
    this.openDialog(UserComponent, dialogConfig);

  }

  onEdit(recordInfo) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.data = { recordInfo };
    this.openDialog(UserComponent, dialogConfig);
  }
  onDelete(id: number) {

    const options = {
      dialogTitle: messages.deleteConfirmHeading,
      message: messages.deleteMsg
    }
    const dialogRef = this.dialogService.openConfirmDialog(options);
    dialogRef.afterClosed().subscribe(closeType => {
      if (closeType && closeType === 'Yes') {
        this.http.delete("users/" + id).subscribe(
          success => {
            this.refreshUserList();
          }
        );
      }
    });
  }

  openDialog(component, dialogConfig) {
    const dialogRef = this.dialog.open(component, dialogConfig);
    this.dialogRefAfterClose(dialogRef);
  }
  dialogRefAfterClose(dialogRef) {
    dialogRef.afterClosed().subscribe(closeType => {
      if (closeType && closeType === 'Success') {
        console.log(closeType);
        this.refreshUserList();
      }
    });
  }

}
