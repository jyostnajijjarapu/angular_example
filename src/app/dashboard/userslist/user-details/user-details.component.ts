import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/services/http/http.service';
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  receivedUserId: string;
  userData: any;
  constructor(private activeRouter: ActivatedRoute, private http: HttpService, private router: Router) {
  }

  ngOnInit(): void {
    this.receivedUserId = this.activeRouter.snapshot.params["userid"];
    this.http.get("users/" + this.receivedUserId).subscribe(
      resultSuccess => {
        this.userData = resultSuccess;
      }
    );
  }
  //redirect to usertableform component
  backTo() {
    this.router.navigate(['dashboard/users_table']);

  }
}
