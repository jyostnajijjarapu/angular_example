import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { messages } from 'src/app/configs/messages';
import { HttpService } from 'src/app/services/http/http.service';
import { LoggerService } from 'src/app/services/logger/logger.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {

  userForm: FormGroup;
  showSpinner = false;
  pageTitle: string;

  get hobbies() {
    return this.userForm.get('hobbies') as FormArray;
  }
  constructor(public http: HttpService,
    private snackbar: MatSnackBar,
    public dialogRef: MatDialogRef<UserComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.userForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      address: new FormGroup({
        city: new FormControl('', [Validators.required]),
        state: new FormControl('', [Validators.required]),
        pincode: new FormControl('', [Validators.required])
      }),
      hobbies: new FormArray([
        new FormControl('', [Validators.required])
      ])
    });
  }



  ngOnInit() {
    if (this.data && this.data.recordInfo) {
      this.userForm.patchValue({ ...this.data.recordInfo });
    }
  }

  // add hobby inputfield
  addHobby() {
    this.hobbies.push(new FormControl('', [Validators.required]));
  }

  // delete hobby
  deleteHobby(i) {
    this.hobbies.removeAt(i);
  }

  // when clicks on submit display the data
  onSubmit() {
    if (this.userForm.valid) {
      this.snackbar.open(messages.loadingDataMsg, null, {
        duration: 2000
      });
      this.showSpinner = true;
      setTimeout(() => {
        this.showSpinner = false;
      }, 2000);

      if (this.data && this.data.recordInfo) {
        this.http.put('users' + '/' + this.data.recordInfo.id, this.userForm.value).subscribe(
          (response) => this.httpSuccessResponse(response),
          (error) => this.httpErrorResponse(error)
        );
      } else {
        this.http.post('users', this.userForm.value).subscribe(
          (response) => this.httpSuccessResponse(response),
          (error) => this.httpErrorResponse(error)
        );
      }
    }
  }



  // To check character is of type number or not
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  onClose() {
    this.dialogRef.close();
  }

  httpSuccessResponse(responseData) {
    this.snackbar.open(messages.loadingsuccess, null, {
      duration: 2000
    });
    LoggerService.print(responseData);
    this.dialogRef.close('Success');

  }

  httpErrorResponse(errorData) {
    this.snackbar.open(messages.serverErrorMsg);
    LoggerService.print(errorData);
  }



}
