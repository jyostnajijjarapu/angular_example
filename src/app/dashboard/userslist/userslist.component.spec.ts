import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Users_tableComponent } from './users_table.component';

describe('UsertableformComponent', () => {
  let component: Users_tableComponent;
  let fixture: ComponentFixture<Users_tableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Users_tableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Users_tableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
