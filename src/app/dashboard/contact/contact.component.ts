import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CanComponentDeactivate } from 'src/app/guards/can-deactivate.guard';
import { DialogService } from 'src/app/services/dialog/dialog.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit, CanComponentDeactivate {
  userContact: FormGroup;
  hasChanges = false;
  constructor(public dialog: MatDialog, public dialogService: DialogService, private router: Router) {
    this.userContact = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
      mobileNo: new FormControl('', Validators.required)
    });

  }
  ngOnInit(): void {
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;


    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    console.log(this.userContact.dirty && !this.userContact.valid);
    if (this.userContact.dirty && !this.userContact.valid) {
      return this.dialogService.canDeactivateConfirm();
    }
    else {
      return true;
    }

  }
  onSave() {

    if (this.userContact.valid) {
      return true;
    }
  }
}
