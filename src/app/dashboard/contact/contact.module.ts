import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ContactComponent } from '../contact/contact.component';
import { CanDeActivateGuard } from 'src/app/guards/can-deactivate.guard';

const routes: Routes = [
    {
        path: '',
        component: ContactComponent,
        canDeactivate: [CanDeActivateGuard]


    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ContactModule { }