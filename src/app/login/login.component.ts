import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginAuthService } from '../services/login-auth/login-auth.service';
// import { DashboardComponent } from '../dashboard/dashboard.component';
import { DialogService } from '../services/dialog/dialog.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  userName: any;
  passWord: any;
  userLoginForm: FormGroup;
  isSubmitted: boolean = false;

  constructor(private route: Router, private loginservice: LoginAuthService) {
    this.userLoginForm = new FormGroup({
      userName: new FormControl('', Validators.required),
      passWord: new FormControl('', Validators.required)
    });
  }
  userLoggedIn() {
    if (this.userLoginForm.valid) {
      this.isSubmitted = true;
      if (this.loginservice.loggedIn()) {
        DialogService.loggedInUser = this.userName;
        this.route.navigate(['/dashboard/home']);

      }
    }
    // console.log(this.userName, this.passWord);
    // if (this.loginservice.loggedIn(this.userName, this.passWord)) {
    //   this.route.navigate(['/dashboard/home']);
    // }

  }
}