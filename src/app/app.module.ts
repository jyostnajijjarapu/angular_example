import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UserComponent } from './dashboard/userslist/user/user.component';
import { UserslistComponent } from './dashboard/userslist/userslist.component';
import { httpInterceptorProviders } from './http-interceptors';
import { AboutComponent } from './dashboard/about/about.component';
import { ContactComponent } from './dashboard/contact/contact.component';
import { UserDetailsComponent } from './dashboard/userslist/user-details/user-details.component';
import { LoginComponent } from './login/login.component';
import { LoginAuthService } from './services/login-auth/login-auth.service';
import { ModalComponent } from './directives/modal/modal.component';
import { LoggerService } from './services/logger/logger.service';
import { ProfileComponent } from './dashboard/profile/profile.component';
@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UserslistComponent,
    AboutComponent,
    ContactComponent,
    UserDetailsComponent,
    LoginComponent,
    ModalComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule, FormsModule, BrowserAnimationsModule, MaterialModule,
    AppRoutingModule, ReactiveFormsModule, HttpClientModule

  ],
  providers: [httpInterceptorProviders, LoginAuthService, LoggerService],
  bootstrap: [AppComponent],
  entryComponents: [UserComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class AppModule { }
