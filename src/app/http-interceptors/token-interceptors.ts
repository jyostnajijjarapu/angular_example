import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClient, HttpErrorResponse
} from '@angular/common/http';

import { Observable, EMPTY, throwError } from 'rxjs';
import { catchError } from 'rxjs/internal/operators/catchError';
import { flatMap, retry } from 'rxjs/operators';
import { api } from '../configs/api';
import { messages } from '../configs/messages';
import { LoggerService } from '../services/logger/logger.service';

// Pass untouched request through to the next request handler.
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private httpClient: HttpClient) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
        LoggerService.print("In TokenInterceptor method")
        return next.handle(req).pipe(
            catchError((err: HttpErrorResponse) => {
                LoggerService.print("Jyostna :" + err);
                throw err;
            })
        );

    }
}

