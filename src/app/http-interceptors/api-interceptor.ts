import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable()
export class APIInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log("In intercept method of APIInterceptor");
        console.log(" url: ", `${environment.baseURL}/${req.url}`)
        const apiReq = req.clone({ url: `${environment.baseURL}/${req.url}` });
        return next.handle(apiReq);
    }

}
