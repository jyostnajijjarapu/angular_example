import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
const materialComponents = [
  MatIconModule, MatFormFieldModule, MatCardModule, MatButtonModule, MatInputModule, MatProgressSpinnerModule,
  MatTableModule, MatSnackBarModule, MatPaginatorModule, MatDialogModule, MatToolbarModule, MatMenuModule, MatListModule, MatTabsModule
]


@NgModule({
  imports: [materialComponents],
  exports: [materialComponents],

})
export class MaterialModule { }
