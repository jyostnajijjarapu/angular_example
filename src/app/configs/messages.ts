export const messages = {
    loadingDataMsg: 'Loading data...',
    serverErrorMsg: 'Something went wrong. Try again later',
    lodingFailedMsg: 'Failed to load data',
    loadingsuccess: 'Loading data successfully.',
    loginFailedMsg: 'Failed to load data',
    logoutMsg: 'Are you sure you want to logout?',
    logoutHeadingMsg: 'Logout',
    deactivateConfirmMsg: 'You have unsaved changes. Are you sure you want to leave?',
    confirmHeading: 'Confirm!',
    deleteConfirmHeading: 'Delete Confirmation',
    deleteMsg: 'Are you sure you want to delete',
    errorHeading: 'Error Message',
};